#образ взятый за основу
FROM openjdk:17
#Записываем в переменную путь до WAR файла
ARG jarFile=target/nmeareader-0.0.1-SNAPSHOT.war
#Куда мы перемещаем варник внутри контейнера
WORKDIR /opt/app
#копируем jar внутрь контейнера
COPY ${jarFile} nmeareader.war
#открываем порт
EXPOSE 9090
#команда для запуска
ENTRYPOINT ["java", "-jar", "nmeareader.war"]