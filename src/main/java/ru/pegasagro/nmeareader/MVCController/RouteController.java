package ru.pegasagro.nmeareader.MVCController;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.pegasagro.nmeareader.model.Route;
import ru.pegasagro.nmeareader.repository.RouteRepository;
import ru.pegasagro.nmeareader.service.RouteService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
@RequestMapping("/routes")
@Slf4j
public class RouteController {
    private final RouteRepository routeRepository;

    private final RouteService routeService;

    public RouteController(RouteService routeService,
                           RouteRepository routeRepository) {
        this.routeService = routeService;
        this.routeRepository = routeRepository;
    }

    @GetMapping("")
    public String index(@RequestParam(value = "page", defaultValue = "1") int page,
                        @RequestParam(value = "size", defaultValue = "10") int size,
                        Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, size, Sort.by(Sort.Direction.ASC, "name"));
        Page<Route> result = routeService.listAllPaginated(pageRequest);
        model.addAttribute("routes", result);
        return "routes/viewAllRoutes";
    }

    @GetMapping("/add")
    public String create() {
        return "routes/addRoute";
    }

    @PostMapping("/add")
    public String create(@RequestParam MultipartFile file) {
        try {
            routeService.uploadFile(file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return "redirect:/routes";
    }

    @GetMapping(value = "/download", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public ResponseEntity<Resource> downloadRouteFile(@Param(value = "RouteId") Long routeId) throws IOException {
        Route route = routeService.getOne(routeId);
        Path path = Paths.get(route.getOnlineCopy());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

        return ResponseEntity.ok()
                .headers(this.headers(path.getFileName().toString()))
                .contentLength(path.toFile().length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }

    private HttpHeaders headers(String name) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + name);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        return headers;
    }

    @GetMapping("delete/{id}")
    public String delete(@PathVariable Long id) {
        try {
            routeService.delete(id);
        }
        catch (Exception exception) {
            log.error(exception.getMessage());
            return "redirect:/error/delete?message=" + exception.getLocalizedMessage();
        }
        return "redirect:/routes";
    }

    @GetMapping("calculate/{id}")
    public String calculate(@PathVariable Long id) {
        try {
            routeService.calculateWay(id);
        } catch (Exception exception) {
            log.error(exception.getMessage());
            return "redirect:/error/calculate?message=" + exception.getLocalizedMessage();
        }
        return "redirect:/routes";
    }

    @GetMapping("/{id}")
    public String viewOneRoute(@PathVariable Long id,
                              Model model) {
        model.addAttribute("route", routeService.getOne(id));
        return "routes/viewRoute";
    }

}