package ru.pegasagro.nmeareader.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pegasagro.nmeareader.model.Route;

@Repository
public interface RouteRepository extends JpaRepository<Route, Long> {
}
