package ru.pegasagro.nmeareader.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "routes")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@SequenceGenerator(name = "default_gen", sequenceName = "routes_seq", allocationSize = 1)
public class Route extends GenericModel {

    @Column(name = "name")
    private String name;

    @Column(name = "way_length")
    private Double wayLength;

    @Column(name = "online_copy")
    private String onlineCopy;
}