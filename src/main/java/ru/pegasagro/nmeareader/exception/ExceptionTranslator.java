package ru.pegasagro.nmeareader.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.webjars.NotFoundException;

@ControllerAdvice
public class ExceptionTranslator {

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorDTO handleMyDeleteException(NotFoundException ex) {
        return processFieldErrors(ex, "Элемент не найден", ex.getMessage());
    }

    private ErrorDTO processFieldErrors(Exception e,
                                        String error,
                                        String description) {
        ErrorDTO errorDTO = new ErrorDTO(error, description);
        errorDTO.add(e.getClass().getName(), "", e.getMessage());
        return errorDTO;
    }
}