package ru.pegasagro.nmeareader.util;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.sf.marineapi.nmea.io.ExceptionListener;
import net.sf.marineapi.nmea.io.SentenceReader;
import net.sf.marineapi.nmea.util.Position;
import net.sf.marineapi.provider.PositionProvider;
import net.sf.marineapi.provider.event.PositionEvent;
import net.sf.marineapi.provider.event.PositionListener;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Getter
@Setter
public class WayCalculator implements PositionListener, ExceptionListener {
    PositionProvider provider;
    Position previousPosition;
    Position currentPosition;
    Boolean hasMorePositionEvents = true;
    int count = 0;
    double distance = 0.0;

    public WayCalculator(File f) throws IOException {
        InputStream stream = new FileInputStream(f);
        SentenceReader reader = new SentenceReader(stream);
        reader.setExceptionListener(this);
        provider = new PositionProvider(reader);
        provider.addListener(this);
        reader.start();
    }

    @Override
    public void onException(Exception e) {
        log.error("WayCalculator#createFile(): {}", e.getMessage());
    }

    @Override
    public void providerUpdate(PositionEvent positionEvent) {
        if (count == 0) {
            previousPosition = positionEvent.getPosition();
        } else {
            if (positionEvent.getSpeed() > 0.0) {
                currentPosition = positionEvent.getPosition();
                distance += currentPosition.distanceTo(previousPosition);
                previousPosition = currentPosition;
            }
        }
        count++;
    }
}