package ru.pegasagro.nmeareader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NmeareaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(NmeareaderApplication.class, args);
    }

}
