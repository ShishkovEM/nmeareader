package ru.pegasagro.nmeareader.service;

import lombok.extern.slf4j.Slf4j;
import net.sf.marineapi.provider.event.PositionEvent;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.data.domain.Page;
import org.webjars.NotFoundException;
import ru.pegasagro.nmeareader.model.Route;
import ru.pegasagro.nmeareader.repository.RouteRepository;
import ru.pegasagro.nmeareader.util.WayCalculator;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class RouteService {

    private final double EPS = 0.00001;

    private static final String UPLOAD_DIRECTORY = "files/logs";

    private final RouteRepository routeRepository;

    public RouteService(RouteRepository routeRepository) {
        this.routeRepository = routeRepository;
    }

    private String createFile(MultipartFile file) {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String resultFileName = "";

        try {
            Path path = Paths.get(UPLOAD_DIRECTORY + "/" + fileName).toAbsolutePath().normalize();
            if (!path.toFile().exists()) {
                Files.createDirectories(path);
            }
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
            resultFileName = UPLOAD_DIRECTORY + "/" + fileName;
        }
        catch (IOException e) {
            log.error("RouteService#createFile(): {}", e.getMessage());
        }
        return resultFileName;
    }

    public void uploadFile(MultipartFile file) throws IOException {
        String resultFileName = createFile(file);
        Route route = new Route();
        route.setName(resultFileName);
        route.setOnlineCopy(resultFileName);
        route.setCreatedWhen(LocalDateTime.now());
        routeRepository.save(route);
    }

    public void calculateWay(Long routeId) throws IOException, InterruptedException {
        Route route = routeRepository.findById(routeId).orElseThrow(
                () -> new NotFoundException("Such route with id=" + routeId + " not found")
        );
        File file = new File(route.getOnlineCopy());

        WayCalculator wayCalculator = new WayCalculator(file);;
        synchronized (this) {
            double d1 = wayCalculator.getDistance();
            while (wayCalculator.getDistance() == d1) {
                wait(30000);
            }
        }
        route.setWayLength(wayCalculator.getDistance());
        routeRepository.save(route);
    }

    public Page<Route> listAllPaginated(Pageable pageRequest) {
        Page<Route> routes = routeRepository.findAll(pageRequest);
        return new PageImpl<>(routes.getContent(), pageRequest, routes.getTotalElements());
    }

    public void delete(Long routeId) {
        Route route = routeRepository.findById(routeId).orElseThrow(
                () -> new NotFoundException("Such route with id=" + routeId + " not found")
        );
        routeRepository.delete(route);
    }

    public Route getOne(Long routeId) {
        return routeRepository.findById(routeId).orElseThrow(
                () -> new NotFoundException("Such route with id=" + routeId + " not found")
        );
    }
}